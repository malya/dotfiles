set t_Co=16
colorscheme slate
syntax on

hi Comment ctermfg=8
hi Include ctermfg=5 cterm=bold
hi Operator cterm=bold ctermfg=12
hi Special ctermfg=1
hi Statement cterm=bold
hi String ctermfg=2
hi Structure ctermfg=4
hi LineNr ctermfg=0
hi CursorLine ctermbg=0 cterm=none
hi CursorLineNr ctermbg=0 cterm=bold
hi ColorColumn ctermbg=0
hi Visual ctermbg=0 cterm=None
hi Search cterm=underline,bold ctermbg=none
hi MatchParen cterm=bold ctermbg=none ctermfg=1
hi Folded ctermfg=6 ctermbg=0
hi Pmenu ctermbg=0 ctermfg=3
hi PmenuSel ctermbg=4 ctermfg=3 cterm=none
hi PmenuSbar ctermbg=8
hi PmenuThumb ctermbg=3
hi StatusLineNC ctermbg=5 ctermfg=16 cterm=none
hi StatusLine ctermbg=2 ctermfg=16 cterm=bold
hi VertSplit ctermfg=5
hi ExtraWhitespace ctermbg=1

set noshowmode
set laststatus=2
hi User1 ctermfg=1 ctermbg=2 cterm=bold
hi User2 ctermfg=3 ctermbg=2 cterm=bold
setl stl=[%{toupper(mode())}]%1*%m%*\ %2*%f%*%=%l/%L\ (%p%%)
au! WinEnter * setl stl=[%{toupper(mode())}]%1*%m%*\ %2*%f%*%=%l/%L\ (%p%%)
au! WinLeave * setl stl=%m\ %f


set nobk nowb noswf
set cul nu cc=80 spr sb
set wildignore=*.pyc
set nowrap et sts=0 ts=4  sw=4 ai
set fdm=indent fdn=2 fdi=
set complete=.,w,b,i


if &term == 'rxvt'
    let &t_SI = "\<Esc>]12;red\x7" " insert mode
    let &t_EI = "\<Esc>]12;white\x7" " normal mode
endif

if has('mouse')
    set mouse=a
    set mousemodel=extend
endif


au! VimResized * :wincmd =
au! BufWritePost $MYVIMRC source %
au! InsertLeave * match ExtraWhitespace /\s\+$/
au! BufWritePre *.* :%s/\s\+$//e


no <F2> :update<CR>
vno <F2> <C-C>:update<CR>
ino <F2> <C-O>:update<CR>

noremap Q <nop>
ino <Ins> <Nop>
vno < <gv
vno > >gv

inoremap <C-h> <Left>
inoremap <C-j> <Down>
inoremap <C-k> <Up>
inoremap <C-l> <Right>
cmap <C-n> <Down>
cmap <C-p> <Up>

cmap w!! w !sudo tee % >/dev/null


iab #! #!/usr/bin/env python
\<CR># -*- coding: utf-8 -*-

iab pdb import pdb
\<CR>pdb.set_trace()




nmap <silent> <F5> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name"). '> trans<' . synIDattr(synID(line("."),col("."),0),"name") . "> lo<" .synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

